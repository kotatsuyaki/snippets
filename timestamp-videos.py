# Python script that uses ffprobe and ffmpeg to put timestamps
# on MOV video files shotted on iPhones.
#
# Author: kotatsuyaki
# License: AGPL-3.0 only
#
# External dependencies: FiraMono-Regular.ttf, ffprobe, ffmpeg

from __future__ import annotations

from datetime import datetime, timedelta
import json
from enum import Enum
import subprocess
import re
from pathlib import Path
from typing import List, cast

class Rotation(Enum):
    NONE = 0,
    CLOCKWISE90 = 1
    ANTICLOCKWISE90 = 2
    HALFROUND = 3

    @staticmethod
    def from_number(number: int | str) -> Rotation:
        number = int(number)
        match number:
            case 0: return Rotation.NONE
            case 90: return Rotation.CLOCKWISE90
            case 270: return Rotation.ANTICLOCKWISE90
            case 180: return Rotation.HALFROUND
            case _: raise RuntimeError("Number %d is not a valid rotation" % number)

    def to_filter_text(self) -> str:
        match self:
            case Rotation.NONE:
                return ""
            case Rotation.CLOCKWISE90:
                return "transpose=1, "
            case Rotation.ANTICLOCKWISE90:
                return "transpose=2, "
            case Rotation.HALFROUND:
                return "vflip, hflip, "

def get_creation_time(filepath: Path):
    proc = subprocess.Popen([
        "ffprobe",
        "-v",
        "quiet",
        "-print_format",
        "json",
        "-show_entries",
        "stream=index,codec_type:stream_tags=creation_time:format_tags=creation_time",
        str(filepath),
    ], stdout=subprocess.PIPE)
    output1, output2 = proc.communicate()
    if proc.returncode != 0:
        raise RuntimeError("Failed running ffprobe:\n%s\n%s" % (output1, output2))
    datetime_str = json.loads(output1)["format"]["tags"]["creation_time"]
    return datetime.strptime(datetime_str, "%Y-%m-%dT%H:%M:%S.%fZ") + timedelta(hours=8)

def stamp_video(filepath: Path, license: str, rotation: Rotation, creation_time: datetime):
    y, m, d, H, M, S = creation_time.year, creation_time.month, creation_time.day, creation_time.hour, creation_time.minute, creation_time.second
    begin_seconds = H * 3600 + M * 60 + S
    drawtext_filter = "drawtext=" + \
        "fontfile=FiraMono-Regular.ttf: " + \
        ("text='%04d-%02d-%02d %%{pts\\:hms\\:%d}': " % (y, m, d, begin_seconds)) + \
        "x=(w-tw)/2: y=h-(2*lh): " + \
        "fontcolor=orange: " + \
        "fontsize=120"
    proc = subprocess.Popen(["ffmpeg", "-i", str(filepath),
                             "-vf", rotation.to_filter_text() + drawtext_filter,
                             "-c:v", "libx265",
                             "-an",
                             "%s.mov" % (license,)])
    output1, output2 = proc.communicate()
    if proc.returncode != 0:
        raise RuntimeError("Failed running ffmpeg:\n%s\n%s" % (output1, output2))

    print("SUCCESS filepath=%s, license=%s" % (str(filepath), license))

if __name__ == "__main__":
    pairs = [
        ("IMG_1234.MOV", "XXX-1234", "0"),
    ]
    last_license = None
    last_file = None
    for file, license, rotation in pairs:
        if last_file == file:
            child = subprocess.Popen(["cp",
                              "--reflink", # CoW on btrfs
                              "%s.mov" % last_license,
                              "%s.mov" % license])
            child.communicate()
            if child.returncode != 0:
                raise RuntimeError("Failed to copy video file")
            continue
        
        creation_time = get_creation_time(Path(file))
        stamp_video(Path(file), license, Rotation.from_number(rotation), creation_time)
        last_file = file
        last_license = license
